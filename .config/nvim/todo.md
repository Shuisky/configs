# List of things to improve in configs

- [x] Remove tildas on empty lines
- [x] Color in hex codes
- [x] Reformat html
- [x] Support for jinja2
- [x] Support to terminal toggle
- [x] Dashboard
- [x] No auto comment on new line
- [ ] Refactoring options for renaming extracting etc
- [x] Control virtual environments
- [x] Toggle term support for lazygit, remove lazygit plugin
- [x] Buffer keybindings
- [ ] GUI configs for neovide
- [ ] Project switcher

# List of worth plugins

- https://github.com/ThePrimeagen/refactoring.nvim Extracting functions and variables
