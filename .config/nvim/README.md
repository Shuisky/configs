# Prerequisites

- fd
- ripgrep
- python
- nvim
- fzf

# Before install

Clean all nvim configs:
`rm $HOME/.config/nvim && rm -rf $HOME/.local/share/nvim && rm -rf $HOME/.local/state/nvim && rm -rf $HOME/.cache/nvim`
Create symlink like this `ln -s ~/code/configs/.config/nvim ~/.config/nvim`

# After install

- run `nvim`
- type `:PackerSync`
- relaunch `nvim`
