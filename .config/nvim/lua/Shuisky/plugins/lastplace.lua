local setup, lastplace = pcall(require, "nvim-lastplace")
if not setup then
	print("lastplace not loaded")
	return
end

lastplace.setup()
