local setup, autopairs = pcall(require, "nvim-autopairs")
if not setup then
	print("autopairs not loaded")
	return
end

autopairs.setup({
	check_ts = true,
	ts_config = {
		lua = { "string" },
		javascript = { "template_string" },
		java = false,
	},
})

-- If you want insert `(` after select function or method item
local cm_setup, cmp_autopairs = pcall(require, "nvim-autopairs.completion.cmp")
if not cm_setup then
	print("autopairs cmp not loaded")
	return
end

local cmp_setup, cmp = pcall(require, "cmp")
if not cmp_setup then
	print("cmp in autopairs not loaded")
	return
end

cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())
