local setup, toggleterm = pcall(require, "toggleterm")
if not setup then
	print("toggleterm not loaded")
	return
end

toggleterm.setup({})

function _G.set_terminal_keymaps()
	local opts = { buffer = 0 }
	vim.keymap.set("t", "<esc>", [[<C-\><C-n>]], opts)
	vim.keymap.set("t", "<A-1>", "<Cmd>ToggleTerm 1 direction=horizontal<CR>", opts)
	vim.keymap.set("t", "<A-2>", "<Cmd>ToggleTerm 2 size=40 direction=vertical<CR>", opts)
	vim.keymap.set("t", "<A-3>", "<Cmd>ToggleTerm 3 direction=float<CR>", opts)
	-- vim.keymap.set("t", "jk", [[<C-\><C-n>]], opts)
	vim.keymap.set("t", "<C-h>", [[<Cmd>wincmd h<CR>]], opts)
	vim.keymap.set("t", "<C-j>", [[<Cmd>wincmd j<CR>]], opts)
	vim.keymap.set("t", "<C-k>", [[<Cmd>wincmd k<CR>]], opts)
	vim.keymap.set("t", "<C-l>", [[<Cmd>wincmd l<CR>]], opts)
end

-- if you only want these mappings for toggle term use term://*toggleterm#* instead
vim.cmd("autocmd! TermOpen term://* lua set_terminal_keymaps()")

local Terminal = require("toggleterm.terminal").Terminal
local lazygit = Terminal:new({
	cmd = "lazygit",
	direction = "float",
	hidden = true,
	on_open = function(term)
		vim.cmd("startinsert!")
		vim.api.nvim_buf_set_keymap(term.bufnr, "n", "<esc>", "<cmd>close<CR>", { noremap = true, silent = true })
	end,
})

function _lazygit_toggle()
	lazygit:toggle()
end

vim.api.nvim_set_keymap(
	"n",
	"<leader>gg",
	"<cmd>lua _lazygit_toggle()<CR>",
	{ desc = "Shows Lazy[G]it interface", noremap = true, silent = true }
)
