local setup, lualine = pcall(require, "lualine")
if not setup then
	print("lualine not loaded")
	return
end

local lualine_nightfly = require("lualine.themes.nightfly")

local new_colors = {
	blue = "#65D1FF",
	green = "#3EFFDC",
	violet = "#FF61EF",
	yellow = "#FFDA7B",
	black = "#000000",
}

lualine_nightfly.normal.a.bg = new_colors.blue
lualine_nightfly.insert.a.bg = new_colors.green
lualine_nightfly.visual.a.bg = new_colors.violet
lualine_nightfly.command = {
	a = {
		gui = "bold",
		bg = new_colors.yellow,
		fg = new_colors.black,
	},
}

local function showTime()
	local time = os.date("*t")
	return ("%02d:%02d"):format(time.hour, time.min)
end

lualine.setup({
	options = {
		theme = lualine_nightfly,
	},
	sections = {
		lualine_c = {
			{
				"filename",
				symbols = {
					modified = "",
					readonly = "",
				},
				color = function(_)
					return { fg = vim.bo.modified and "#ff5874" or "#c3ccdc" }
				end,
			},
		},
		lualine_x = { { "filetype" } },
		lualine_y = { { "progress" }, "location" },

		lualine_z = { showTime },
	},
	extensions = { "nvim-tree", "toggleterm" },
})
