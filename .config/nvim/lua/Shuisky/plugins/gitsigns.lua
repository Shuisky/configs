local setup, gitsigns = pcall(require, "gitsigns")
if not setup then
	print("gitsigns not loaded")
	return
end

gitsigns.setup()
