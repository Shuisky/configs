local ensure_packer = function()
	local fn = vim.fn
	local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
	if fn.empty(fn.glob(install_path)) > 0 then
		fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path })
		vim.cmd([[packadd packer.nvim]])
		return true
	end
	return false
end

local packer_bootstrap = ensure_packer()

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins-setup.lua source <afile> | PackerSync
  augroup end
]])

local status, packer = pcall(require, "packer")
if not status then
	return
end

return packer.startup(function(use)
	-- main plugin to manage other plugins
	use("wbthomason/packer.nvim")

	-- profiling plugins loading
	use("lewis6991/impatient.nvim")

	-- library for other plugins
	use("nvim-lua/plenary.nvim")

	-- Colorschemes
	use("bluz71/vim-nightfly-guicolors")

	-- tmux and split window navigation
	use("christoomey/vim-tmux-navigator")

	-- maximize window
	use("szw/vim-maximizer")

	-- essential plugins
	use("tpope/vim-surround")
	use("tpope/vim-repeat")
	use("vim-scripts/ReplaceWithRegister")
	use("ggandor/lightspeed.nvim")

	-- commenting with gc
	use("numToStr/Comment.nvim")

	-- icons
	use("nvim-tree/nvim-web-devicons")

	-- file explorer
	use("nvim-tree/nvim-tree.lua")

	-- status line
	use("nvim-lualine/lualine.nvim")

	-- Telescope
	use({ "nvim-telescope/telescope.nvim", branch = "0.1.x" })
	use({ "nvim-telescope/telescope-fzf-native.nvim", run = "make" })

	-- lsp-zero
	use({
		"VonHeikemen/lsp-zero.nvim",
		branch = "v2.x",
		requires = {
			-- LSP Support
			{ "neovim/nvim-lspconfig" }, -- Required
			{ "williamboman/mason.nvim" }, -- Optional
			{ "williamboman/mason-lspconfig.nvim" }, -- Optional

			-- Autocompletion
			{ "hrsh7th/nvim-cmp" }, -- Required
			{ "hrsh7th/cmp-nvim-lsp" }, -- Required
			{ "L3MON4D3/LuaSnip" }, -- Required
		},
	})

	-- treesitter
	use({
		"nvim-treesitter/nvim-treesitter",
		run = function()
			require("nvim-treesitter.install").update({ with_sync = true })
		end,
	})

	-- auto closing
	use("windwp/nvim-autopairs")

	-- git integration
	use("lewis6991/gitsigns.nvim")

	-- helpers
	use("NvChad/nvim-colorizer.lua")
	use("ethanholz/nvim-lastplace")
	use({ "abecodes/tabout.nvim", wants = { "nvim-treesitter" }, after = { "nvim-cmp" } })
	use("tpope/vim-abolish") -- substitude preserving case
	use("RRethy/vim-illuminate") -- highlight words under cursor

	--dashboard
	use("goolord/alpha-nvim")

	-- terminal
	use({ "akinsho/toggleterm.nvim", tag = "*" })

	if packer_bootstrap then
		require("packer").sync()
	end
end)
