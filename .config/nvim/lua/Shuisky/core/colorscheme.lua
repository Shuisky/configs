vim.g.nightflyTransparent = true
vim.g.nightflyWinSeparator = 2 -- windows separation as line
vim.opt.fillchars = {
	horiz = "━",
	horizup = "┻",
	horizdown = "┳",
	vert = "┃",
	vertleft = "┫",
	vertright = "┣",
	verthoriz = "╋",
	eob = " ",
}
vim.g.nightflyCursorColor = true

local status, _ = pcall(vim.cmd, "colorscheme nightfly")
if not status then
	print("Colorscheme not found")
	return
end
