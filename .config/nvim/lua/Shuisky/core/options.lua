local o = vim.opt
local g = vim.g

-- appearance
o.termguicolors = true
-- o.guicursor = ""
o.background = "dark"
o.signcolumn = "yes"
o.fillchars = "eob: "

--colorcolumn settings
vim.cmd([[
autocmd filetype python setlocal colorcolumn=80
]])

-- line numbers
o.number = true
o.relativenumber = true

-- tabs and indentation
o.tabstop = 4
o.softtabstop = 4
o.shiftwidth = 4
o.expandtab = true
o.smartindent = true
o.autoindent = true

-- search settings
o.hlsearch = true
o.incsearch = true
o.ignorecase = true
o.smartcase = true

-- line wrapping
o.wrap = true

-- cursor line
o.cursorline = false
o.swapfile = false
o.undofile = true

-- backspace
o.backspace = "indent,eol,start"

-- clipboard
o.clipboard:append("unnamedplus")

-- split windows
o.splitright = true
o.splitbelow = true

-- symbols count as one word
o.iskeyword:append("-")

-- no comment new string
vim.cmd([[
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
]])

-- scroll
o.scrolloff = 5
o.sidescrolloff = 5

-- gui config
o.guifont = { "Fira Code", ":h22" }
g.neovide_transparency = 0.9
g.neovide_fullscreen = true
g.neovide_cursor_vfx_mode = "railgun"

-- highlight yanked text for 200ms using the "Visual" highlight group
vim.cmd([[
augroup highlight_yank
autocmd!
au TextYankPost * silent! lua vim.highlight.on_yank({higroup="Visual", timeout=200})
augroup END
]])
