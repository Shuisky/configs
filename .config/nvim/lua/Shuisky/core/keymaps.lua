vim.g.mapleader = " "

local k = vim.keymap

k.set("n", "<leader>w", ":w<CR>", { noremap = true, desc = "[W]rite. Save file/buffer" })
k.set("n", "<leader>q", ":q<CR>", { noremap = true, desc = "[Q]uit buffer. Exit nvim" })
k.set("n", "<leader>fs", ":so %<CR>", { noremap = true, desc = "[F]ile [S]ource. Load this file in nvim" })

k.set("i", "jk", "<Esc>", { desc = "Type [j][k] to enter normal from insert mode" })
k.set("n", "<leader>nh", ":nohl<CR>", { desc = "[N]o [H]ighlight" })
k.set("n", "x", '"_x', { desc = "x does not replace buffer" })

k.set("n", "<leader>=", "<C-a>", { desc = "Increase number. `=` is `+`" })
k.set("n", "<leader>-", "<C-x>", { desc = "Decrease number" })

k.set("n", "<leader>sv", "<C-w>v", { desc = "[S]plit [V]ertical" })
k.set("n", "<leader>sh", "<C-w>s", { desc = "[S]plit [H]orizontal" })
k.set("n", "<leader>se", "<C-w>=", { desc = "Resize [S]plits [E]qual" })
k.set("n", "<leader>sx", ":close<CR>", { desc = "Close [S]plit. E[X]it split" })

k.set("n", "<leader>to", ":tabnew<CR>", { desc = "New [T]ab. [O]pen new tab" })
k.set("n", "<leader>tx", ":tabclose<CR>", { desc = "Close [T]ab. E[X]it tab" })

k.set("n", "<leader>tn", ":tabn<CR>", { desc = "Go to [T]ab [N]ext to current" })
k.set("n", "<S-l>", ":tabn<CR>", { desc = "Go to tab next to current. l - right" })

k.set("n", "<leader>tp", ":tabp<CR>", { desc = "Go to [T]ab [P]revious to current" })
k.set("n", "<S-H>", ":tabp<CR>", { desc = "Go to tab previous to current. h - left" })

k.set("n", "<leader>x", ":bd<CR>", { desc = "E[X]it buffer" })
k.set("n", "<leader>k", ":bn<CR>", { desc = "Next buffer. k - up" })
k.set("n", "<leader>j", ":bp<CR>", { desc = "Previous buffer. j - down" })

k.set("v", "J", ":m '>+1<CR>gv=gv", { desc = "Move selected lines down. j - down" })
k.set("v", "K", ":m '<-2<CR>gv=gv", { desc = "Move selected lines up. k - up" })

k.set("n", "J", "mzJ`z", { desc = "Cursor stays on the same place while [J]oining" })

k.set("n", "<C-d>", "<C-d>zz", { desc = "Stay on center while jumping half page down" })
k.set("n", "<C-u>", "<C-u>zz", { desc = "Stay on center while jumping half page up" })

k.set("n", "n", "nzzzv", { desc = "Center next word while searching" })
k.set("n", "N", "Nzzzv", { desc = "Center next word while searching backwards" })

-- plugins keymaps

-- vim-maximizer
k.set("n", "<leader>sm", ":MaximizerToggle<CR>", { desc = "[S]plit [M]aximize" })

-- nvim-tree
k.set("n", "<leader>e", ":NvimTreeToggle<CR>", { desc = "Shows tree [E]xplorer" })
k.set("n", "<leader>.", ":NvimTreeFocus<CR>", { desc = "Focus tree explorer" })

-- Show alpha dashboard
k.set("n", "<leader>;", ":Alpha<CR>", { desc = "Show dashboard" })

-- Telescope
k.set("n", "<leader>ff", "<cmd>Telescope find_files<CR>", { desc = "[F]ind [F]ile in current directory" })
k.set("n", "<leader>fg", "<cmd>Telescope live_grep<CR>", { desc = "[F]iles [G]rep. Search for string in directory" })
k.set("n", "<leader>fc", "<cmd>Telescope grep_string<CR>", { desc = "[F]ind string in [C]urrent buffer" })
k.set("n", "<leader>fb", "<cmd>Telescope buffers<CR>", { desc = "[F]ind opened buffer" })
k.set("n", "<leader><space>", "<cmd>Telescope buffers<CR>", { desc = "Find opened buffer" })
k.set("n", "<leader>fh", "<cmd>Telescope help_tags<CR>", { desc = "[F]ind [H]elp" })
k.set("n", "<leader>fk", "<cmd>Telescope keymaps<CR>", { desc = "[F]ind [K]eymaps" })

-- terminal toggle
k.set("n", [[<c-\>]], ":ToggleTermToggleAll<CR>", { desc = "Toggle all terminals" })
k.set("n", "<A-1>", ":ToggleTerm 1 direction=horizontal<CR>", { desc = "Toggle horizontal terminal" })
k.set("n", "<A-2>", ":ToggleTerm 2 size=40 direction=vertical<CR>", { desc = "Toggle vertical terminal" })
k.set("n", "<A-3>", ":ToggleTerm 3 direction=float<CR>", { desc = "Toggle floating terminal" })
