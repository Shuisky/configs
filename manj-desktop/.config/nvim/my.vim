" cSpell:disable
call plug#begin('~/.vim/plugged')
" color schemes
Plug 'morhetz/gruvbox'

" A fuzzy file finder
Plug 'ctrlpvim/ctrlp.vim'
" Comment/Uncomment tool
Plug 'scrooloose/nerdcommenter'
Plug 'itchyny/lightline.vim'
" Better syntax-highlighting for filetypes in vim
" Plug 'sheerun/vim-polyglot'
" Intellisense engine
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Git integration
Plug 'tpope/vim-fugitive'
"Plug 'tpope/vim-rhubarb'

" Auto-close braces and scopes
Plug 'jiangmiao/auto-pairs'

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update

Plug 'lewis6991/gitsigns.nvim'

Plug 'stsewd/gx-extended.vim' " Goto extended gx

call plug#end()
" cSpell:enable
set pyxversion=3
set encoding=utf-8
set hidden
set scrolloff=8
set noshowmode
set mouse=a

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smartindent

set number
set numberwidth=4
set relativenumber

set nobackup
set nowritebackup
set noswapfile
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set nohlsearch
set ignorecase
set smartcase
set nowrap
set splitbelow
set splitright

set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=number
set clipboard+=unnamedplus
colorscheme gruvbox
set background=dark

filetype plugin indent on
syntax on

let mapleader = ","
vmap <leader>a <Plug>(coc-codeaction-selected)<CR>
nmap <leader>a <Plug>(coc-codeaction-selected)<CR>
" Format action on <leader>f
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
" Goto definition
nmap <silent> gd <Plug>(coc-definition)
" Show references
nmap <silent> gr <Plug>(coc-references)
" Open definition in a split window
nmap <silent> gv :vsp<CR><Plug>(coc-definition)<C-W>L


set guifont=Meslo\ LG\ M\ for\ Powerline:h20
