#!/usr/bin/env bash

nitrogen --restore &
picom --experimental-backends &
