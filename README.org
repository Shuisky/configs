#+title: Knowledge Base

* Git repository
link https://gitlab.com/Shuisky/configs
** How to create symlinks
*** Windows
use `mklink`
#+begin_src shell
mklink Link Target
#+end_src
example: Run command prompt as administrator
#+begin_src shell
mklink C:\Users\%username%\AppData\Roaming\.doom.d C:\Users\%username%\git\configs\.doom.d
#+end_src
